package com.example.polewebsitebackend;

import jdk.jfr.ContentType;
import org.springframework.boot.WebApplicationType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class PoleWebsiteController {
    @GetMapping("/home")
    public ModelAndView viewHomePage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("LandingPage.html");
        return modelAndView;
    }

    @GetMapping("/login")
    public ModelAndView showLoginSignUpForms(Model model) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("LoginSignUp.html");
        return modelAndView;
    }
}