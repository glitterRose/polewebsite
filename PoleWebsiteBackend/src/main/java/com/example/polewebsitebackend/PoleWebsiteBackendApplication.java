package com.example.polewebsitebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoleWebsiteBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(PoleWebsiteBackendApplication.class, args);
    }

}
